﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedPlatform : MonoBehaviour
{
    private GameObject _player;
    private CapsuleCollider _cc;
    private Rigidbody _rb;
    private bool _isDestroy;

    private Vector3 _lastCamPos;
    private Transform _cameraPos;
    private float _offsetForCamCheck = 6f;


    private void Awake()
    {
        _rb = GetComponent<Rigidbody>();
        _rb.isKinematic = true;
        _cc = GetComponent<CapsuleCollider>();
        _cc.isTrigger = true;
        _player = GameObject.FindGameObjectWithTag("Player");
        _cameraPos = Camera.main.transform;
        _lastCamPos = _cameraPos.position;
    }

    void Update()
    {
        Vector3 playerPos = _player.transform.position;

        if (playerPos.y >= transform.position.y && _isDestroy == false)
            _cc.isTrigger = false;

        if (transform.position.y + _offsetForCamCheck < _cameraPos.position.y)
            Destroy(gameObject);

        if (_cameraPos.position.y < _lastCamPos.y)
            Destroy(gameObject);
        _lastCamPos = _cameraPos.position;
    }

    void OnCollisionEnter(Collision collision)
    {
        _rb.isKinematic = false;
        _cc.isTrigger = true;
        _isDestroy = true;
    }
}
