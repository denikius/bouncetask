﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YellowPlatform : Platform
{
    private float _offsetX = 2f;
    private Vector3 _startPos;
    private Vector3 _target;
    private Vector3 currentVelocity;
    private bool _right;
    private float _speed = 2f;

    void Start()
    {
        _right = true;
        _startPos = transform.position;

        _target = transform.position;
        if (transform.position.x < 0)
            _offsetX *= -1f;
        _target.x = _offsetX;
    }

    void LateUpdate()
    {
        transform.position = Vector3.MoveTowards(transform.position,
                                                 _target,
                                                 _speed * Time.deltaTime);

        if (_right == true && transform.position == _target)
        {
            _offsetX *= -1f;
            _target.x = _offsetX;
            _right = false;
        }
        if (_right == false && transform.position == _target)
        {
            _offsetX *= -1f;
            _target.x = _offsetX;
            _right = true;
        }
    }
}
