﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField]
    private Manager manager;
    public Manager Manager => manager;

    private Vector3 _firstPos;
    private Vector3 _currentPos;
    private float _offsetForCamCheck = 8f;
    private Transform _cameraPos;


    void Awake()
    {
        _cameraPos = Camera.main.transform;
        enabled = false;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _firstPos = Input.mousePosition;
        }
        if (Input.GetMouseButton(0))
        {
            _currentPos = Input.mousePosition;
            float x = _currentPos.x - _firstPos.x;
            Vector3 newPos = transform.position;
            newPos.x += x / Screen.currentResolution.width;
            transform.position = newPos;
        }

        if (transform.position.x <= -3.9f)
        {
            Vector3 pos = transform.position;
            pos.x = 3.8f;
            transform.position = pos;
        }
        if (transform.position.x >= 3.9f)
        {
            Vector3 pos = transform.position;
            pos.x = -3.8f;
            transform.position = pos;
        }

        if (transform.position.y + _offsetForCamCheck < _cameraPos.position.y)
            manager.GameOver();
    }
}
