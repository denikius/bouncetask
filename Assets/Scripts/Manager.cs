﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Manager : MonoBehaviour
{
    [SerializeField]
    private GameObject _playButton;
    [SerializeField]
    private GameObject _pauseButton;
    [SerializeField]
    private GameObject _continueButton;
    [SerializeField]
    private GameObject _restartButton;
    [SerializeField]
    private GameObject _panel;

    [SerializeField]
    private GameObject _scoreTextGO;
    [SerializeField]
    private GameObject _bestScoreTextGO;
    [SerializeField]
    private Text _scoreGO;
    [SerializeField]
    private Text _bestScoreGO;



    [SerializeField]
    private Player _player;


    [SerializeField]
    private Text _scoreText;
    private int _score;
    private int _bestScore;


    void Awake()
    {
        _score = 0;
        _scoreText.text = _score.ToString();

        _bestScore = PlayerPrefs.GetInt("bestScore", _bestScore);
    }

    public void ChangeScore()
    {
        _score++;
        _scoreText.text = _score.ToString();

        _scoreGO.text = _scoreText.text;
    }

    public void PlayButton()
    {
        _playButton.SetActive(false);

        Vector3 gravity = Physics.gravity;
        gravity.y = -30f;
        Physics.gravity = gravity;

        _player.enabled = true;
        _pauseButton.SetActive(true);
    }

    public void PauseButton()
    {
        _pauseButton.SetActive(false);
        _panel.SetActive(true);
        _continueButton.SetActive(true);
        _restartButton.SetActive(true);

        Time.timeScale = 0f;
    }

    public void ContinueButton()
    {
        _pauseButton.SetActive(true);
        _panel.SetActive(false);
        _continueButton.SetActive(false);
        _restartButton.SetActive(false);

        Time.timeScale = 1f;
    }

    public void RestartButton()
    {
        _scoreText.gameObject.SetActive(true);
        _pauseButton.SetActive(true);
        _panel.SetActive(false);
        _continueButton.SetActive(false);
        _restartButton.SetActive(false);

        _scoreTextGO.SetActive(false);
        _bestScoreTextGO.SetActive(false);
        _scoreGO.gameObject.SetActive(false);
        _bestScoreGO.gameObject.SetActive(false);
               
        Time.timeScale = 1f;

        _player.transform.position = Vector3.zero;
        _player.GetComponent<Rigidbody>().velocity = Vector3.zero;
        Camera.main.transform.position = new Vector3(0f, 1f, -10f);
    }

    public void GameOver()
    {
        if (_score > _bestScore)
            _bestScore = _score;
        PlayerPrefs.SetInt("bestScore", _bestScore);

        _bestScoreGO.text = _bestScore.ToString();

        _scoreText.gameObject.SetActive(false);
        _pauseButton.SetActive(false);
        _panel.SetActive(true);
        _restartButton.SetActive(true);
        _scoreTextGO.SetActive(true);
        _bestScoreTextGO.SetActive(true);
        _scoreGO.gameObject.SetActive(true);
        _bestScoreGO.gameObject.SetActive(true);

        
               
        Time.timeScale = 0f;

        _score = 0;
        _scoreText.text = _score.ToString();
    }

}
