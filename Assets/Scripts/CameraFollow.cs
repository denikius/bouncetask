﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    private GameObject _target;


    private void Awake()
    {
        _target = GameObject.FindGameObjectWithTag("Player");
    }

    void Update()
    {
        if (_target.transform.position.y > transform.position.y)
        {
            transform.position = new Vector3(transform.position.x,
                                             _target.transform.position.y,
                                             transform.position.z);
        }

    }
}
