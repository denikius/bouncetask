﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    [SerializeField]
    private GameObject[] _platforms;
    [SerializeField]
    private GameObject _redPlatform;

    private int _numberOfPlatforms = 5;
    private float _width = 2.25f;
    private float _minY = 3f;
    private float _maxY = 5f;

    private float _lastYPos = 6f;
    private float _offsetForCamCheck = 10f;
    private Transform _camera;

    private Vector3 _lastCamPos;


    void Awake()
    {
        _camera = Camera.main.transform;
        _lastCamPos = _camera.position;
        GeneratePlatforms();
    }

    private void Update()
    {
        if (_camera.position.y + _offsetForCamCheck >= _lastYPos)
            GeneratePlatforms();

        if (_camera.position.y < _lastCamPos.y)
            _lastYPos = 6f;
        _lastCamPos = _camera.position;
    }

    private void GeneratePlatforms()
    {
        Vector3 spawnPos = new Vector3();
        Vector3 spawnPosRedPlatform = new Vector3();

        for (int i = 0; i < _numberOfPlatforms; i++)
        {
            _lastYPos += Random.Range(_minY, _maxY);
            spawnPos.y = _lastYPos;
            spawnPos.x = Random.Range(-_width, _width);
            int idPlatform = Random.Range(0, _platforms.Length);

            Instantiate(_platforms[idPlatform], spawnPos, Quaternion.identity);

            if (i % Random.Range(2, 5) == 0f)
            {
                spawnPosRedPlatform = spawnPos;
                int offset = Random.Range(-1, 1);
                if (offset == 0)
                    offset = 1;
                spawnPosRedPlatform.y += offset;
                spawnPosRedPlatform.x = Random.Range(-_width, _width);
                Instantiate(_redPlatform, spawnPosRedPlatform, Quaternion.identity);
            }

            _lastYPos = spawnPos.y;
        }
    }
}
