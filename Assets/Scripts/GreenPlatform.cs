﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GreenPlatform : Platform
{
    private float _offsetY = 1.5f;
    private Vector3 _startPos;
    private Vector3 _target;
    private Vector3 currentVelocity;
    private bool _up;
    private float _speed = 2f;

    void Start()
    {
        _up = true;
        _startPos = transform.position;

        int direction = Random.Range(-1, 1);
        if (direction == 0)
            direction = 1;

        _target = transform.position;
        _target.y = _startPos.y + _offsetY * direction;
    }

    void LateUpdate()
    {
        transform.position = Vector3.MoveTowards(transform.position,
                                                 _target,
                                                 _speed * Time.deltaTime);

        if (_up == true && transform.position == _target)
        {
            _offsetY *= -1f;
            _target.y = _startPos.y + _offsetY;
            _up = false;
        }
        if (_up == false && transform.position == _target)
        {
            _offsetY *= -1f;
            _target.y = _startPos.y + _offsetY;
            _up = true;
        }
    }
}
