﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
    private float _collisionOffset = 0.3f;

    private GameObject _player;
    private CapsuleCollider _cc;
    private float _jumpForce = 18f;
    private Transform _cameraPos;
    private float _offsetForCamCheck = 6f;
    private Vector3 _lastCamPos;
    private bool _changeScore = true;


    private void Awake()
    {
        _cc = GetComponent<CapsuleCollider>();
        _cc.isTrigger = true;
        _player = GameObject.FindGameObjectWithTag("Player");
        _cameraPos = Camera.main.transform;
        _lastCamPos = _cameraPos.position;
    }

    void Update()
    {
        Vector3 playerPos = _player.transform.position;

        if (playerPos.y + _collisionOffset <= transform.position.y)
            _cc.isTrigger = true;
        else
            _cc.isTrigger = false;

        if (transform.position.y + _offsetForCamCheck < _cameraPos.position.y &&
            transform.position.y > 7f)
            Destroy(gameObject);


        if (_cameraPos.position.y < _lastCamPos.y &&
            transform.position.y > 7f)
            Destroy(gameObject);
        _lastCamPos = _cameraPos.position;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.relativeVelocity.y <= 0f)
        {
            Rigidbody rb = collision.rigidbody;
            Vector3 velocity = rb.velocity;
            velocity.y = _jumpForce;
            rb.velocity = velocity;

            if (_changeScore == true && transform.position.y > 7f)
            {
                Player player = collision.gameObject.GetComponent<Player>();
                player.Manager.ChangeScore();
                _changeScore = false;
            }
        }
    }
}
